#import <React/RCTBridgeModule.h>
#import <React/RCTEventEmitter.h>

@interface RCT_EXTERN_MODULE(CropImageService, RCTEventEmitter)
RCT_EXTERN_METHOD(cropToBounds: (nonnull NSString *)strBase64
                  widthImage:(nonnull NSNumber *)widthImage
                  heightImage:(nonnull NSNumber *)heightImage
                  posX:(nonnull NSNumber *)posX
                  posY:(nonnull NSNumber *)posY)
@end
