@objc(CropImageService)
class CropImageService: RCTEventEmitter {
  
  @objc(cropToBounds:widthImage:heightImage:posX:posY:)
  func cropToBounds(_ strBase64: String, widthImage: NSNumber, heightImage: NSNumber, posX: NSNumber, posY: NSNumber) {
    let cgwidth: CGFloat = CGFloat(truncating: widthImage)
    let cgheight: CGFloat = CGFloat(truncating: heightImage)
    let rect: CGRect = CGRect(x: CGFloat(truncating: posX), y: CGFloat(truncating: posY), width: cgwidth, height: cgheight)
    if let dataDecoded : Data = Data(base64Encoded: strBase64, options: .ignoreUnknownCharacters),
       let image = UIImage(data: dataDecoded),
       let cgimage = image.cgImage,
       let imageRef: CGImage = cgimage.cropping(to: rect) {
      let image: UIImage = UIImage(cgImage: imageRef, scale: image.scale, orientation: image.imageOrientation)
      debugPrint(convertImageToBase64String(image))
      sendEvent(withName: MethodName.REVERSE_DATA, body: convertImageToBase64String(image))
    } else {
      sendEvent(withName: MethodName.REVERSE_DATA, body: "")
    }
  }
  
  func convertImageToBase64String (_ img: UIImage) -> String {
    let imageData:NSData = img.jpegData(compressionQuality: 0.50)! as NSData //UIImagePNGRepresentation(img)
    let imgString = imageData.base64EncodedString(options: .init(rawValue: 0))
    return imgString
  }
  
  override static func requiresMainQueueSetup() -> Bool {
    return true
  }
  
  override func supportedEvents() -> [String]! {
    return [MethodName.REVERSE_DATA]
  }
}
